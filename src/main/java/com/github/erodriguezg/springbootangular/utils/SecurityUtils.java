package com.github.erodriguezg.springbootangular.utils;

import org.springframework.security.core.context.SecurityContextHolder;

import com.github.erodriguezg.springbootangular.security.Identidad;

public class SecurityUtils {

    public Identidad getActualIdentidad() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication != null && authentication.getPrincipal() != null) {
            return (Identidad) authentication.getPrincipal();
        }
        return null;
    }

}
